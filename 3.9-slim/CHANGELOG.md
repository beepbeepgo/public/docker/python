## python-3.9-slim [1.0.1](https://gitlab.com/beepbeepgo/public/docker/python/compare/python-3.9-slim@1.0.0...python-3.9-slim@1.0.1) (2022-12-30)


### Bug Fixes

* test new references ([1c2c857](https://gitlab.com/beepbeepgo/public/docker/python/commit/1c2c85727e96088a0b25cf9cb81bc297725c8a86))

# CHANGELOG

## python-3.9-slim 1.0.0 (2022-12-16)

### Features

* init release ([bfd1f97](https://gitlab.com/beepbeepgo/public/docker/python/commit/bfd1f9775a4ae3edcbd16ea5b8553798f4f13e7c))
